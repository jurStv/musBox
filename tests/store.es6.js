import test from 'tape';
import I from 'immutable';

import init from '../src/store/init.es6.js';
import update from '../src/store/update.es6.js';
import Actions from '../src/actions/actionTypes.es6.js';
import SocketActions from '../src/actions/socketActions.es6.js';

test('testing initial state', t => {
  t.plan(6);
  const initialState = init();

  t.ok(I.Map.isMap(initialState), 'initial state must be immutable Map')
  t.notOk(initialState.get('displayingPlayer'), 'displayingPlayer must be false')
  t.equal(initialState.get('currentPlaying'), null, 'currentPlaying must be null')
  t.ok(I.List.isList(initialState.get('posts')), 'posts must be immutable Set')
  t.ok(I.List.isList(initialState.get('currentPlaylist')), 'currentPlaylist must be immutable Set')
  t.equal(initialState.get('columns'), 3, 'columns number must be 3')

  t.end()
})

test('testing update function', t => {
  t.plan(5);
  const initialState = init().set('posts', I.fromJS([{x: 101}, {y: 0}]));
  const testArr = [{a:1, b:2},{a:3, b:4, c:5}];

  const updated1 = initialState.set('isPlayerShown', true);
  t.ok(I.is(updated1, update(Actions.DisplayPlayer(), initialState)), 'show player property must be true, others stay same')
  t.ok(I.is(initialState, update(Actions.HidePlayer(), updated1)), 'show player property must be false, others stay same')

  const updated2 = initialState.set('currentPlaylist', I.fromJS(testArr));
  t.ok(I.is(updated2, update(Actions.UpdatePlaylist(testArr), initialState)), 'playlist property must contain new data, others stay same')

  const updated3 = initialState.update('posts', posts => I.fromJS(testArr).concat(posts));
  const updated4 = initialState
    .update('posts', posts => posts.merge(testArr))
    .update('currentPlaylist', playlists => playlists.merge(testArr));

  t.ok(I.is(updated3, update(Actions.UpdatePosts(SocketActions.GetUpdatePosts(testArr)), initialState)), 'posts property must contain new data, others stay same')
  t.ok(I.is(updated4, update(Actions.UpdatePosts(SocketActions.GetInitialPosts(testArr)), initialState)), 'posts property and playlist property must contain new data, others stay same')

  t.end();
})
