const pmongo = require('promised-mongo');
const rp = require('request-promise');
const co = require('co');
const R = require('ramda');
// Posts stuff
const sortSC = require('../helpers/sortSC');
const sortVK = require('../helpers/sortVK');
const requestsVK = (ownerID, count) => {
  const params = [
    'owner_id=-'+ownerID,
    'extended=1',
    'fields=attachments',
    'count='+count,
    'v=5.37'
  ];
  return 'https://api.vk.com/method/wall.get.json?'+params.join('&');
}
const setOptions = (uri) => ({json: true, uri: uri});
const handlePosts = (cb) => R.compose(R.filter(R.compose(R.not, R.isNil)), R.map(cb), R.unnest)
const handleSc = handlePosts(sortSC);
const handleVk = R.compose(handlePosts(sortVK), R.map(R.path(['response','items'])));
// Sources stuff
const scpages = require('./scpages');
const vkIds = require('./vkIds');
const resolve = (url, id) =>
  'http://api.soundcloud.com/resolve?url='+url+'&client_id='+id
const vk = 'https://api.vk.com/method/groups.getById.json?group_ids='+vkIds.join(',');
const setOptions = (uri) => ({json: true, uri: uri})
const modifySc = (x) => ({avatar_url: x.avatar_url, id: x.id, username:  x.username, apiURI: x.uri, from: 'sc'})
const modifyVk = (x) => ({avatar_url: x.photo_medium, id: x.gid, username: x.name, uri_name: x.screen_name, from: 'vk'})
// Gets all stuff, posts and sources and writes it to database
function* getAllStuff() {
  try {
    const db = pmongo('localhost/musBox_dnb', ['posts', 'sources', 'info'])
    db.info.insert({type:'sc_id', _id: 'a373cba1bb0392bf3cba96c827e2383b'})
    const info = yield db.info.findOne({type: "sc_id"})
    // Get sources
    const returned1 = yield scpages.map(adr => rp(setOptions(resolve(adr, info._id))));
    const scProfiles = returned1.map(modifySc);
    const returned2 = yield [rp(setOptions(vk))];
    const vkProfiles = returned2[0].response.map(modifyVk);
    const profiles = scProfiles.concat(vkProfiles);
    yield db.sources.drop();
    yield db.sources.insert(profiles);
    // Get posts
    const vkResponses = yield vkProfiles.map(x => rp(setOptions(requestsVK(x.id, 100))));
    const scTrackResponses = yield scProfiles.map(x => rp(setOptions(x.apiURI+'/tracks?client_id='+info._id)))
    const scPlaylistResponses = yield scProfiles.map(x => rp(setOptions(x.apiURI+'/playlists?client_id='+info._id)))
    const responses = R.uniqWith((x, y) => x._id === y._id,
      handleVk(vkResponses).concat(handleSc(scTrackResponses.concat(scPlaylistResponses))))
    const posts = R.splitEvery(500, responses)
    yield db.posts.drop();
    for(var i = 0; i < posts.length; i++) {
      yield db.posts.insert(posts[i])
    }
  }
  catch(e) {
    console.log(e)
  }
  finally {
    db.close();
  }
}

co(getAllStuff).catch(e => console.log(e))
