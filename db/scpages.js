const ids = [
  'https://soundcloud.com/atmoteka',
  'https://soundcloud.com/paradox-music-uk',
  'https://soundcloud.com/absysrecords',
  'https://soundcloud.com/noisymeditation',
  'https://soundcloud.com/deep-sound-channel',
  'https://soundcloud.com/dust',
  'https://soundcloud.com/paluca',
  'https://soundcloud.com/dysfunkmusic',
  'https://soundcloud.com/soulredrecords',
  'https://soundcloud.com/alphacut',
  'https://soundcloud.com/kontext',
  'https://soundcloud.com/audio-evil',
  'https://soundcloud.com/breakbeat-rebelz',
  'https://soundcloud.com/blokone',
  'https://soundcloud.com/djbooca',
  'https://soundcloud.com/eschatonmusic',
  'https://soundcloud.com/monochrome-recordings',
  'https://soundcloud.com/hidden-hawaii',
  'https://soundcloud.com/nitri',
  'https://soundcloud.com/nucleus',
  'https://soundcloud.com/djtrax',
  'https://soundcloud.com/sciwax'
]

module.exports = ids;
