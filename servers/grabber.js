'use strict';

const rufus = require('../helpers/rufusModule');
const logger = rufus.getLogger('grabber');
const errLogger = rufus.getLogger('grabber.errors');
const devLogger = rufus.getLogger('grabber.stats');

const axon = require('axon');
const pub = axon.socket('pub-emitter');
pub.connect(8887);

const memCheck = require('../helpers/memoryCheck');
memCheck(60*12, 60*14, devLogger);

const R = require('ramda');
const sortSC = require('../helpers/sortSC');
const sortVK = require('../helpers/sortVK');

const requestsVK = (ownerID, count) => {
  const params = [
    'owner_id=-'+ownerID,
    'extended=1',
    'fields=attachments',
    'count='+count,
    'v=5.37'
  ];
  return 'https://api.vk.com/method/wall.get.json?'+params.join('&');
}

const rp = require('request-promise');
const co = require('co');
const setOptions = (uri) => ({timeout: 30*1000, json: true, uri: uri});

const handlePosts = (cb) => R.compose(R.filter(x => !!x), R.map(cb),R.filter(x => !!x), R.unnest)
const handleSc = handlePosts(sortSC);
const handleVk = R.compose(handlePosts(sortVK), R.map(R.path(['response','items'])));

const DbHelper = require('../helpers/DbHelper');
const db = new DbHelper(
  () => {
    logger.info("Db connection has been opened.");
    co(grab).catch(e => errLogger.error(e.message, JSON.stringify(e), e.stack))}
);

const sendRequestWithDelay = (url) => new Promise((resolve, reject)=>{
  setTimeout(() => rp(setOptions(url)).then(x => resolve(x)).catch(e => {
    logger.info('resolved with error: '+e.message)
    resolve([])
  }), 500)
})

function* requestFromSourcesVk(sources) {
  let posts = [];
  let newPost;
  for (let i = 0; i < sources.length; i++) {
    newPost = yield sendRequestWithDelay(requestsVK(sources[i].id, 20));
    posts = R.append(newPost, posts);
  }
  return handleVk(posts)
}

function* requestFromSourcesSc(sources, type) {
  const id = db.info.id;
  let posts = [];
  let newPost;
  for (let i = 0; i < sources.length; i++) {
    if (type === 'tracks')
      newPost = yield sendRequestWithDelay(requestsVK(sources[i].apiURI+'/tracks?client_id='+id));
    else if (type === 'playlists')
      newPost = yield sendRequestWithDelay(requestsVK(sources[i].apiURI+'/playlists?client_id='+id));
    posts = R.append(newPost, posts);
  }
  return handleSc(posts)
}


function* grab() {
  try {
    const sources = db.getSources();
    const vkSources = sources.filter(x => x.from === 'vk');
    const scSources = sources.filter(x => x.from === 'sc');
    if(R.is(String, sources)) throw new Error(sources);
    logger.info("Starting requests...")

    const vkResponses = yield* requestFromSourcesVk(vkSources);
    const scTrackResponses = yield* requestFromSourcesSc(scSources, 'tracks');
    const scPlaylistResponses = yield* requestFromSourcesSc(scSources, 'playlists');
    const posts = vkResponses.concat(scTrackResponses).concat(scPlaylistResponses);

    const dbPosts = db.getPosts();
    const uniqPosts = R.uniqWith((x, y) => x._id === y._id,
        posts.filter(post => !dbPosts.some( x => x._id === post._id)))
    if(uniqPosts.length > 0)  {
      yield db.addNewPosts(uniqPosts);
      logger.info('Post has been added, and db instance updated.');
      pub.emit('updates', uniqPosts);
    }
    else {
      logger.info('There is no uniq posts.')
    }
  }
  catch (e) {
    errLogger.error(e.message, JSON.stringify(e), e.stack)
  }
}



setInterval(
  () => co(grab).catch(e => errLogger.error(e.message, JSON.stringify(e), e.stack))
  , 20*60*1000
);
