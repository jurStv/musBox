'use strict';

/* DEV SETUP */
const rufus = require('../helpers/rufusModule');
const logger = rufus.getLogger('server.info');
const errLogger = rufus.getLogger('server.errors');
const devLogger = rufus.getLogger('server.stats');

const memCheck = require('../helpers/memoryCheck');
const port = process.env.PORT || 3000;
const ENV = process.env.NODE_ENV;
const isDev = ENV === 'development';
logger.info("NODE_ENV is set to: %s", ENV);
if (ENV === 'development') memCheck(60*20, 60*25, devLogger);

/* EXPRESS, SOCKETIO, AXON (FOR GRABBER SERVER SUBSCRIPTION), DATABASE HELPER MODULES */

const express = require('express');
const app = express();
const server = require('http').Server(app);
const compression = require('compression');

const socketIO = require('socket.io');
const io = socketIO(server);

const axon = require('axon');
const sub = axon.socket('sub-emitter');
sub.bind(8887);



/* SERVER STUFF */

app.set('view engine', 'jade')
  .use(compression())
  .get('/favicon.ico', (req, res) => res.end())
  .use(express.static('./public'))
  .get('*', (req, res) => {
     res.render('index', {
      dev: isDev,
      cssHref: '/css/styles.css',
      scriptSrc: isDev ? '/js/bundle.js' : '/js/bundle.min.js',
    })
  });

const transformDates = require('../helpers/transformDates');
const DbHelper = require('../helpers/DbHelper');
const posts = new DbHelper(_ => {
  server.listen(port, (error) =>  error ? err(error) :
    logger.info('==> 🌎 Open up http://192.168.56.101:%s/ ', port)
  )
}, transformDates);



/* SOCKET STUFF */

io.sockets.on('connection', socket => {
  socket.emit('startPosts', posts.getPosts(30));
  socket.on('getMorePosts', skip => socket.emit('loadMorePosts', posts.getPosts(30, skip)));
})

/* PUBSUB GRABBER STUFF */

sub.on('updates', newPosts => {
  logger.info('Update received from grabber, sending it to all sockets.');
  io.sockets.emit('updatePosts', newPosts);
  posts.updateData();
})
