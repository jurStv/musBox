'use strict';

const path = require('path')
const pName = path.basename(module.parent.filename, '.js');

const rufus = require('./rufusModule');
const logger = rufus.getLogger('dbHelper.'+pName);
const errLogger = rufus.getLogger('dbHelper.errors.'+pName);

const pmongo = require('promised-mongo');
const co = require('co');
const GETPOSTS = Symbol();
const ADD_SIGINT_HANDLER = Symbol();
const RUN_GENERATOR = Symbol();
const GETSOURCES = Symbol();
const DB = Symbol();
const POSTS = Symbol();
const SOURCES = Symbol();
const MIDDLEWARE = Symbol();


class DbHelper {
  constructor(cb, mw) {
    logger.info('creating instance of DbHelper');
    this[DB] = pmongo('localhost/musBox_dnb', ['posts', 'sources', 'info']);
    this.connected = true;
    this[MIDDLEWARE] = mw || null;

    if(cb) {
      this.updateData().then(cb).catch(e => {errLogger.error(e); this[DB].close();});
    }
    else this.updateData();
    this[ADD_SIGINT_HANDLER]();
  }
  *[GETPOSTS]() {
      const posts = yield this[DB].posts.find().sort({date: -1}).toArray();
      this[POSTS] = this[MIDDLEWARE] ? this[MIDDLEWARE](posts) : posts;
      return posts;
  }
  *[GETSOURCES]() {
    const sources = yield this[DB].sources.find().toArray();
    this[SOURCES] = sources;
    return sources;
  }
  *[RUN_GENERATOR]() {
    const posts = yield this[GETPOSTS]();
    const sources = yield this[GETSOURCES]();
    this.info = yield this[DB].info.findOne({type: "sc_id"});
    return {posts: posts, sources: sources};
  }
  [ADD_SIGINT_HANDLER]() {
    process.on('SIGINT', () => {
      this[DB].close();
      process.exit();
    });
  }
  *addNewPosts(posts) {
    yield this[DB].posts.insert(posts);
    yield this[RUN_GENERATOR]();
  }
  getPosts(count, from) {
    if (!this[POSTS]) return "Posts doesn't ready yet, try a bit later...";
    if (!this.connected) return "Sorry database is disconnected...";
    from = from || 0;
    count = count || this[POSTS].length;
    return this[POSTS].slice(from, from + count);
  }
  getSources() {
    if (!this[SOURCES]) return "Sources doesn't ready yet, try a bit later...";
    if (!this.connected) return "Sorry database is disconnected...";
    return this[SOURCES];
  }
  updateData() {
    this[POSTS] = null;
    this[SOURCES] = null;
    return co(this[RUN_GENERATOR].bind(this))
      .catch(e => {errLogger.error(e); this[DB].close();})
  }
}

module.exports = DbHelper;
