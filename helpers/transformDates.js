var moment = require('moment');
var map = require('ramda/src/map');
var evolve = require('ramda/src/evolve');

const transformDates = map(evolve({ date: time => moment.unix(time).calendar() }));

module.exports = transformDates;
