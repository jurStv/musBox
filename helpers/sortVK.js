module.exports = function sortVK(post){
    if (post.copy_history){
      return sortVK(post.copy_history[0])
    }
    else if (post.attachments){
      var date = post.date;
      var photos = post.attachments.filter(function(attach){ return attach.type === "photo" });
      var audios = post.attachments.filter(function(attach){ return attach.type === "audio" });
      var type = audios.length === 1 ? 'track' : 'playlist';
      var data = audios.map(function(audio){
        return {
          title: audio.audio.title,
          artist: audio.audio.artist,
          duration: audio.audio.duration,
          streamUrl: audio.audio.url,
        }
      });
      if (audios.length === 0){
        return null;
      }
      return {
        data: type === 'track' ? data[0] : data,
        metadata: {
          cover: photos[0] ? photos[0].photo.photo_604 : 'https://pp.vk.me/c629421/v629421836/24844/Sb-9y59WN9o.jpg',
          type: type,
          sourceFrom: 'vk',
        },
        date: date,
        _id: 'VK' + type + post.id,
      }
    }
    else {
      return null;
    }
}
