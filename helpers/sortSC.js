var moment = require('moment');
module.exports = function sortSC(post){
  var data = post.kind === 'track' ? {
    title: post.title,
    artist: post.user.username,
    duration: post.duration,
    streamUrl: post.stream_url,
  } : post.kind === 'track' ?
    post.tracks.map(track => sortSC(track)).map(track => track.data) : null;
  if (!data) return null;
  return {
    data: data,
    metadata: {
      cover: post.artwork_url || 'https://pp.vk.me/c629421/v629421836/24844/Sb-9y59WN9o.jpg',
      type: post.kind,
      sourceFrom: 'sc',
      link: post.permalink_url
    },
    date: moment(post.created_at, "YYYY-MM-DD HH:mm:ss").unix(),
    _id: 'SC' + post.kind + post.id,
  }
}
