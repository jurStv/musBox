module.exports = function(x,y, logger){
  const megabyte = 1024 * 1024;
  logger = logger || console.log.bind(console);
  setInterval(function() {
    logger.info(
      "MemoryUsage: " + Math.ceil(process.memoryUsage().heapUsed / megabyte * 100) / 100
    );
  }, 1000*x);
  setInterval(function(){
    global.gc();
    logger.info('GC done.')
  }, 1000*y);
}
