import _globalStyles from './styles.css';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import init from './store/init.es6';
import store from './store/index.es6';
import source from './store/actionSource.es6';
import Actions from './actions/actionTypes.es6.js';

import App from './containers/App.es6';

ReactDOM.render(
  <App store={store} initial={init} source={source} Actions={Actions} />,
  document.getElementById('app')
);
