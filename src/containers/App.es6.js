import React, { Component, PropTypes } from 'react';
import R from 'ramda';
import Rx from 'rx';
import I from 'immutable';
import styles from './App.css';
import Display from './Display.es6';
import Post from '../components/Post.es6';
import Message from '../actions/MessageTypes.es6.js';

const sortToColumns = R.curry((n, _, a) => a % n);

export default class App extends Component {
  static propTypes = {
    Actions: PropTypes.object.isRequired,
    initial: PropTypes.instanceOf(I.Map).isRequired,
    source: PropTypes.instanceOf(Rx.Observable).isRequired,
    store: PropTypes.instanceOf(Rx.Observable).isRequired,
  }
  constructor(props, context) {
    super(props, context);
    this.state = { iss: this.props.initial };
  }

  /* SERVICE METHODS */

  componentDidMount() {
    setTimeout(_ => this.subscribe(), 1000);
    
  }
  componentWillUnmount() {
    this.unsubscribe();
  }

  /* CUSTOM PROPS AND METHODS */

  storeSubscribtion = null
  audio = null
  loadMore = () => {
    if(this.storeSubscribtion && !this.state.iss.get('loading')) {
      this.props.source.onNext(this.props.Actions.RequestMorePosts());
    }
  }
  subscribe = () => {
    if(!this.storeSubscribtion) {
      this.storeSubscribtion = this.props.store
        .doAction(x => console.log(x.toJS()))
        .subscribe(
          newState => this.setState({ iss: newState }),
          err => console.log(err)
        );
    }
  }
  play = () => {
    if (!this.audio.playing()) {
      this.audio.play();
    }
  }
  pause = () => {
    if (this.audio.playing()) {
      this.audio.pause();
    }
  }
  hideMessage = () => {
    if(this.storeSubscribtion) {
      this.props.source.onNext(this.props.Actions.HideMessage());
    }
  }
  reloadPage = () => document.location.reload(true)
  renderMessage = R.flip(Message.caseOn({
    Error: (text) =>
      (<p>
        {text}<br/>{'Click refresh '}
        <i onClick={this.reloadPage} className={'fa fa-refresh ' + styles.refreshButton}></i>
      </p>),
    Notification: R.identity,
    Empty: R.always(null),
  }))(null)

  render() {
    let posts = this.state.iss.get('posts');
    let message = this.renderMessage(this.state.iss.get('message'));
    let loading = this.state.iss.get('loading');
    let colNum = this.state.iss.get('columns');
    return (
      <div className={styles.mainContainer}>
        <Display visible={!!message}>
          <div onClick={this.hideMessage} className={styles.messageBoard}>
            {message || 'This message should not be displayed!'}
          </div>
        </Display>
        <h1>{'Listen this amazing sounds...'}</h1>
        <button className={styles.button} onClick={this.play}>PLAY</button>
        <button className={styles.button} onClick={this.pause}>PAUSE</button>
        <Display visible={posts.size !== 0}>
          <hr className={styles.separator} />
          <div className={styles.container}>
            {posts.groupBy(sortToColumns(colNum)).map((columnPosts, i) =>
              <div key={i} className={styles['column' + colNum]}>
                {columnPosts.map((post, j) => <Post key={j} post={post} n={`${i}.${j}`} />).toJS()}
              </div>
            ).toArray()}
          </div>
          <hr className={styles.bottomSeparator} />
          <button className={styles.loadMoreButton} onClick={this.loadMore}>
            {loading ? 'Loading...' : 'Load more posts'}
          </button>
        </Display>
      </div>
    );
  }
}
