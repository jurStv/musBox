import React, { Component, PropTypes } from 'react';

export default class Display extends Component {
  static propTypes = {
    children: PropTypes.any,
    visible: PropTypes.bool.isRequired,
  }
  render() {
    return (
      <div>
        {this.props.visible ? this.props.children : ''}
      </div>
    );
  }
}
