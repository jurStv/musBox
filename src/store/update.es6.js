import SocketActions from '../actions/socketTypes.es6.js';
import Actions from '../actions/actionTypes.es6.js';
import I from 'immutable';
import socket from './sock.es6';
import Message from '../actions/MessageTypes.es6.js';

/* SOCKET UPDATE METHODS */

const getInitialPosts =
  (posts, model) => {
    let newPosts = I.fromJS(posts);
    return model
      .set('posts', newPosts)
      .set('currentPlaylist', newPosts)
      .set('loading', false)
      .set('message', Message.Empty());
  };

const getUpdatePosts =
  (newPosts, model) =>
    model.update('posts',
      posts => I.fromJS(newPosts).concat(posts)
    );

const loadMorePost =
  (newPosts, model) =>
    model.update('posts',
      posts => posts.concat(I.fromJS(newPosts))
    ).set('loading', false);

/* MAIN UPDATE METHODS */

const requestMorePosts =
  (model) => {
    if(socket.connected) {
      socket.emit('getMorePosts', model.get('posts').size);
      return model.set('loading', true);
    }
    return model.set('message', Message.Error('Error! Socket has been disconnected. Try refresh this page.'));
  };

const updatePlaylist =
  (newPosts, model) =>
    model.update('currentPlaylist',
      playlist => playlist.clear().merge(newPosts)
    );

/* UPDATE FUNCTIONS */

const sockUpdate = SocketActions.caseOn({
  GetInitialPosts: getInitialPosts,
  GetUpdatePosts: getUpdatePosts,
  LoadMorePost: loadMorePost,
  ShowMessage: (message, model) => model.set('message', message).set('loading', false),
});

const update = Actions.caseOn({
  SocketActions: sockUpdate,
  UpdatePlaylist: updatePlaylist,
  DisplayPlayer: (model) => model.set('isPlayerShown', true),
  HidePlayer: (model) => model.set('isPlayerShown', false),
  HideMessage: (model) => model.set('message', Message.Empty()),
  ShowMessage: (msg, model) => model.set('message', msg),
  RequestMorePosts: requestMorePosts,
  ChangeColNumber: (number, model) => model.set('columns', number),
});

export default update;
