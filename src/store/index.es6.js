import R from 'ramda';
import Rx from 'rx';

import socket from './sock.es6';
import init from './init.es6';
import source from './actionSource.es6';
import update from './update.es6';

import SocketActions from '../actions/socketTypes.es6.js';
import Actions from '../actions/actionTypes.es6.js';
import Message from '../actions/MessageTypes.es6.js';

const fromEvent = (sock, event) => Rx.Observable.fromEventPattern(h => sock.on(event, h));

/* SOCKET EVENT SOURCES */

const startPost$ =
  fromEvent(socket, 'startPosts')
    .take(1)
    .map(
      data =>
        (R.type(data) === 'String') ? SocketActions.ShowMessage(Message.Error(data)) :
        (R.type(data) === 'Array') ? SocketActions.GetInitialPosts(data) : null
    )
    .filter(x => !!x);
const updatePost$ =
  fromEvent(socket, 'updatePosts').map(posts => SocketActions.GetUpdatePosts(posts));

const loadMorePost$ =
  fromEvent(socket, 'loadMorePosts')
    .map(
      data =>
        (R.type(data) === 'String') ? SocketActions.ShowMessage(Message.Error(data)) :
        (R.type(data) === 'Array') ? SocketActions.LoadMorePost(data) : null
    )
    .filter(x => !!x);

const disconnect$ =
  fromEvent(socket, 'disconnect')
    .doAction(_ => socket.close())
    .map(_ => SocketActions.ShowMessage(Message.Error('Socket has been disconnected. Try refresh this page.')));

/* STORE, SCANNING ALL SOURCES */

const store =
  startPost$
    .merge(updatePost$)
    .merge(loadMorePost$)
    .merge(disconnect$)
    .map(sockAct => Actions.SocketActions(sockAct))
    .merge(source)
    .scan(R.flip(update), init)
    .replay(null, 1);

store.connect();

export default store;
