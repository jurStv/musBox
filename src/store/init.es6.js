import I from 'immutable';
import R from 'ramda';
import Message from '../actions/MessageTypes.es6.js';

const getColumnsNumber = (w) => ((w < 640) ? 2 : (w < 1024) ? 3 : (w < 1466) ? 4 : (w < 1820) ? 5 : 6);

const init = I.fromJS({
  isPlayerShown: false,
  currentPlaying: null,
  posts: [],
  currentPlaylist: [],
  loading: true,
  columns: getColumnsNumber(typeof window !== 'undefined' ? window.innerWidth : 1300),
}).set('message', Message.Notification('loading...'));

export default init;
