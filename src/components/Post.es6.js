import React, { Component, PropTypes } from 'react';
import styles from './Post.css';
import I from 'immutable';

export default class Post extends Component {
  static propTypes = {
    post: PropTypes.instanceOf(I.Map).isRequired
  }
  
  render() {
    return (
      <img src={this.props.post.getIn(['metadata','cover'])} className={styles.image} />
    )
  }

}
