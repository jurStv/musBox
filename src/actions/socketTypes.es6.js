import Type from 'union-type';
import Message from './MessageTypes.es6';

export default Type({
  GetInitialPosts: [Array],
  GetUpdatePosts: [Array],
  LoadMorePost: [Array],
  ShowMessage: [Message],
});
