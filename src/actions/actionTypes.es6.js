import Type from 'union-type';
import SocketActionTypes from './socketTypes.es6';
import Message from './MessageTypes.es6';
import PlayerActionTypes from './playerTypes.es6';

export default Type({
  PlayerActions: [PlayerActionTypes],
  SocketActions: [SocketActionTypes],
  UpdatePlaylist: [Array],
  DisplayPlayer: [],
  HidePlayer: [],
  HideMessage: [],
  ShowMessage: [Message],
  RequestMorePosts: [],
  ChangeColNumber: [Number],
});
