import Type from 'union-type';

export default Type({
  Error: [String],
  Notification: [String],
  Empty: [],
});
