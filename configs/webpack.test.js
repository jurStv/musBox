var path = require('path');
var webpack = require('webpack');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');
var find = require('find');
var files = find.fileSync(/\.es6\.js$/, '../tests').reduce(function(acc, file){
  var name = path.basename(file, '.es6.js');
  acc[name] = '../'+file;
  return acc;
 }, {})

var config = {
  entry: files,
  output: {
    path: path.join(__dirname, '..', "tests" ,"build"),
    filename: "[name].build.js",
    libraryTarget: "umd"
  },
  plugins: [
    new ProgressBarPlugin(),
  ],
  target: "node",
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: [
          path.resolve(__dirname, '..', "node_modules"),
        ],
        query: {
          presets: ['es2015'],
        }
      }
    ]
  },
  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['','.js'],
  }
};

module.exports = config;
