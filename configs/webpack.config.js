var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');

var config = {
  entry: [
    './src/index.es6.js'
  ],
  cache: true,
  output: {
    path: path.join(__dirname, '..', 'public', 'js'),
    filename: 'bundle.js',
    libraryTarget: "umd"
  },
  plugins: [
    new ProgressBarPlugin(),
    new webpack.DllReferencePlugin({
      context: path.join(__dirname, '..'),
      manifest: require("../dll/all-manifest.json")
    }),
    new ExtractTextPlugin('../css/styles.css', { allChunks: true }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
    {
      test: /\.es6\.js$/,
      loader: 'babel',
      exclude: [
        path.resolve(__dirname, '..', "node_modules"),
      ],
      query: {
        plugins: ['transform-runtime'],
        presets: ['es2015', 'stage-0', 'react', 'stage-1'],
      }
    },
    {
      test: /\.css$/,
      exclude: [
        path.resolve(__dirname, '..', "node_modules"),
      ],
      loader: ExtractTextPlugin.extract('style', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss')
    }
  ]
  },
  postcss: [
    require('postcss-import'),
		require('postcss-extend'),
		require('postcss-css-variables'),
		require('postcss-custom-media'),
		require('postcss-media-minmax'),
		require('rucksack-css'),
		require('autoprefixer')
  ],
  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['','.js'],
  }
};

if (process.env.NODE_ENV  !== 'development') {
  config.output.filename = 'bundle.min.js';
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
        warnings: false
    },
    sourceMap: false
  }));
}
else {
  var LiveReloadPlugin = require('webpack-livereload-plugin');
  config.plugins.push(new LiveReloadPlugin())
  config.watch = true
  config.devtool = 'cheap-module-eval-source-map';
}
module.exports = config;
