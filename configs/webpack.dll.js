var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
  entry: {
        all: ["react", "rx", "ramda", "immutable", 'socket.io-client', 'union-type' ]
    },
  cache: true,
  output: {
        path: path.join(__dirname, '..',"public", "js" ,"dll"),
        filename: "MyDll.[name].js",
        library: "[name]_[hash]"
    },
  plugins: [
        new webpack.DllPlugin({
            path: path.join(__dirname, '..',"dll", "[name]-manifest.json"),
            name: "[name]_[hash]"
        }),
        new webpack.optimize.UglifyJsPlugin({
          compress: {
              warnings: false
          }
        })
    ]
};

module.exports = config;
